"""
Copyright 2020 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
At runtime, this script will include all the
.py files in __all__ so that the package import *
functionility needed for things like menu generation
will include all the contained class/modules.
"""
from pathlib import Path as _Path


__all__ = [e.name.replace('.py', '') for e in _Path(__file__).parent.glob('*.py') if not e.name.startswith('_')]

