Jabir is the security and filtering extension to NoRD.

This extension is so named as it renders nodes and edges inert. Inert being a chemistry term, and Jabir ibn Hayyan is considered the father
of chemistry.... 

Walujapi uses Jabir to decide whether or not to run nodes ar follow edges. 

Sigurd uses Jabir when deciding whether or not to show nodes, edges etc.

Wrigan uses Jabir for access and content control

Pasion integrates with Jabir to enforce fund availability (or lack thereof).

